package com.example.demo.hello;

import com.example.demo.hello.HelloService;
import com.example.demo.lang.Lang;
import com.example.demo.lang.LangRepository;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HelloServiceTest
{
    private final static String WELCOME = "Hello";

    private LangRepository getMockLangRepository()
    {
        return new LangRepository()
        {
            @Override
            public Optional<Lang> findById(Integer id)
            {
                return Optional.of(new Lang(null, WELCOME, null));
            }
        };
    }

    @Test
    public void test_prepareGreeting_nullName_returnsGreetingWithFallbackName() throws Exception
    {
        // given
        LangRepository mockRepository = getMockLangRepository();
        HelloService SUT = new HelloService(mockRepository);
        String name = null;

        // when
        var result = SUT.prepareGreeting(null, -1);

        // then
        assertEquals(WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
    }

    @Test
    public void test_prepareGreeting_name_returnsGreetingWithName() throws Exception
    {
        // given
        LangRepository mockRepository = getMockLangRepository();
        HelloService SUT = new HelloService(mockRepository);
        String name = "test";

        // when
        var result = SUT.prepareGreeting(name, -1);

        // then
        assertEquals(WELCOME + " " + name + "!", result);
    }

    @Test
    public void test_prepareGreeting_nullLang_returnsGreetingWithFallbackIdLang() throws Exception
    {
        // given
        var fallbackIdWelcome = "Hola";
        LangRepository mockRepository = new LangRepository()
        {
            @Override
            public Optional<Lang> findById(Integer id)
            {
                if (id.equals(HelloService.FALLBACK_LANG.getId()))
                    return Optional.of(new Lang(null, fallbackIdWelcome, null));
                return Optional.empty();
            }
        };
        HelloService SUT = new HelloService(mockRepository);

        // when
        var result = SUT.prepareGreeting(null, null);

        // then
        assertEquals(fallbackIdWelcome + " " + HelloService.FALLBACK_NAME + "!", result);
    }

    /*@Test
    public void test_prepareGreeting_nonNumericLang_returnsGreetingWithFallbackIdLang() throws Exception
    {
        // given
        LangRepository mockRepository = getMockLangRepository();
        HelloService SUT = new HelloService(mockRepository);
        String name = null;

        // when
        var result = SUT.prepareGreeting(null, "abc");

        // then
        assertEquals(WELCOME + " " + HelloService.FALLBACK_NAME + "!", result);
    }*/

    @Test
    public void test_prepareGreeting_noExistingLang_returnsGreetingWithFallbackIdLang() throws Exception
    {
        // given
        LangRepository mockRepository = new LangRepository();
        HelloService SUT = new HelloService(mockRepository);
        String name = "test";

        // when
        var result = SUT.prepareGreeting(name, -1);

        // then
        assertEquals(WELCOME + " " + name + "!", result);
    }
}
