package com.example.demo.lang;

import com.example.demo.HibernateUtil;

import java.util.List;
import java.util.Optional;

public class LangRepository
{
    List<Lang> findAll()
    {
        var sessionFactory = HibernateUtil.getSessionFactory();
        var session = sessionFactory.openSession();
        var transaction = session.beginTransaction();

        var result = session.createQuery("from Lang", Lang.class).list();

        transaction.commit();
        session.close();

        return result;
    }

    public Optional<Lang> findById(Integer id)
    {
        var session = HibernateUtil.getSessionFactory().openSession();
        var transaction = session.beginTransaction();

        var result = session.get(Lang.class, id);

        transaction.commit();
        session.close();

        return Optional.ofNullable(result);
    }
}
