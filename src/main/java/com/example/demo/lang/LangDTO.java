package com.example.demo.lang;

public class LangDTO
{
    public Integer id;
    public String code;

    LangDTO(Lang lang)
    {
        this.id = lang.getId();
        this.code = lang.getCode();
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }
}
