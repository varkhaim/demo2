package com.example.demo.lang;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

class LangService
{
    LangRepository repository;

    List<LangDTO> findAll()
    {
//        var langList = repository.findAll();
//        List<LangDTO> dtosToReturn = new ArrayList<LangDTO>();
//
//        for (Lang lang : langList)
//        {
//            dtosToReturn.add(new LangDTO(lang));
//        }

        return repository.findAll().stream().map(LangDTO::new).collect(Collectors.toList());
    }

    LangService(LangRepository repository)
    {
        this.repository = repository;
    }

    public LangService()
    {
        this(new LangRepository());
    }
}
