package com.example.demo.hello;

import com.example.demo.lang.Lang;
import com.example.demo.lang.LangRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

class HelloService
{
    static final String FALLBACK_NAME = "world";
    static final Lang FALLBACK_LANG = new Lang(1, "Hello", "en");
    private final Logger logger = LoggerFactory.getLogger(HelloService.class);

    private LangRepository repository;

    HelloService()
    {
        this (new LangRepository());
    }

    public HelloService(LangRepository repository)
    {
        this.repository = repository;
    }


    String prepareGreeting(String name)
    {
        return prepareGreeting(name, null);
    }

    String prepareGreeting(String name, Integer langId)
    {
        langId = Optional.ofNullable(langId).orElse(FALLBACK_LANG.getId());

        var welcomeMsg = GetLangById(langId).getWelcomeMsg();
        var nameToWelcome = Optional.ofNullable(name).orElse(FALLBACK_NAME);
        return welcomeMsg + " " + nameToWelcome + "!";
    }

    private Lang GetLangById(Integer langId)
    {
        Lang returnedLang;
        returnedLang = repository.findById(langId).orElse(null);
        if (returnedLang == null)
        {
            logger.warn("Given language (" + langId + ") doesn't exists");
            returnedLang = FALLBACK_LANG;
        }
        return returnedLang;
    }
}
