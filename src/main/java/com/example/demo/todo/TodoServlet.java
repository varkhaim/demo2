package com.example.demo.todo;

import com.example.demo.lang.LangRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Todo", urlPatterns = {"/api/todos/*"})
public class TodoServlet extends HttpServlet
{
    private final Logger logger = LoggerFactory.getLogger(TodoServlet.class);
    private TodoRepository repository;
    private ObjectMapper mapper;

    /**
     * This is needed
     */

    @SuppressWarnings("unused")
    public TodoServlet()
    {
        this(new TodoRepository(), new ObjectMapper());
    }

    TodoServlet(TodoRepository repository, ObjectMapper mapper)
    {
        this.mapper = mapper;
        this.repository = repository;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        logger.info("Got request with parameters: " + req.getParameterMap());
        resp.setContentType("application/json;charset=UTF-8");
        mapper.writeValue(resp.getOutputStream(), repository.findAll());
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Integer todoId;
        var idText = req.getPathInfo().substring(1);
        log(idText);
        try
        {
            todoId = Integer.parseInt(idText);
        }
        catch (NumberFormatException e)
        {
            logger.warn("Todo Id is not valid number: " + idText);
            todoId = -1;
        }
        mapper.writeValue(resp.getOutputStream(), repository.toggleTodo(todoId));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        Todo newTodo;

        newTodo = mapper.readValue(req.getInputStream(), Todo.class);
        var debugId = newTodo.getId();
        var debugText = newTodo.getText();
        var debugDone = newTodo.getDone();
        logger.info(debugId + debugText + debugDone);
        resp.setContentType("application/json;charset=UTF-8");
        mapper.writeValue(resp.getOutputStream(), repository.addTodo(newTodo));
    }
}
