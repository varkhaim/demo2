package com.example.demo.todo;

import com.example.demo.HibernateUtil;

import java.util.List;
import java.util.Optional;

public class TodoRepository
{
    List<Todo> findAll()
    {
        var sessionFactory = HibernateUtil.getSessionFactory();
        var session = sessionFactory.openSession();
        var transaction = session.beginTransaction();

        var result = session.createQuery("from Todo", Todo.class).list();

        transaction.commit();
        session.close();

        return result;
    }

    public Optional<Todo> findById(Integer id)
    {
        var session = HibernateUtil.getSessionFactory().openSession();
        var transaction = session.beginTransaction();

        var result = session.get(Todo.class, id);

        transaction.commit();
        session.close();

        return Optional.ofNullable(result);
    }

    public Todo toggleTodo(Integer id)
    {
        var session = HibernateUtil.getSessionFactory().openSession();
        var transaction = session.beginTransaction();

        Todo result = session.get(Todo.class, id);

        transaction.commit();
        session.close();

        result.setDone(!result.getDone());
        return result;
    }

    Todo addTodo(Todo newTodo)
    {
        var session = HibernateUtil.getSessionFactory().openSession();
        var transaction = session.beginTransaction();

//        var debugId = newTodo.getId();
//        var debugText = newTodo.getText();
//        var debugDone = newTodo.getDone();
//        var temp = debugId + debugText + debugDone;
        session.persist(newTodo);

        transaction.commit();
        session.close();

        return newTodo;
    }
}
